<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LOGIN | Batalla Naval</title>
    <link rel="stylesheet" type="text/css" href="/public/css/estilos.css">
</head>
<body>
<center>
    @if(isset($estatus))
        @if($estatus == "success")
            <label class="text-success">{{$mensaje}}</label>
        @elseif($estatus == "error")
            <label class="text-warning">{{$mensaje}}</label>
        @endif
    @endif
</center>
<form class="user" action="{{route('login.form')}}" method="post">
    {{csrf_field()}}
    <h1>Acceso a usuarios</h1>
    <div class="inputContenedor">
        <i class="fas fa-envelope"></i>
        <input type="email" placeholder="Correo" name="correo">
    </div>
    <div class="inputContenedor">
        <i class="fas fa-key"></i>
        <input type="password" placeholder="Contraseña" name="password">
    </div>
    <input type="submit" value="Ingresar" class="button" name="registrar">
    <p>¿No tienes cuenta? <a class="link" href="{{route('registro')}}">Crear cuenta</a></p>
</form>
</body>
</html>
